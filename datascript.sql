CREATE DATABASE `lesson_80`;
USE `lesson_80`;

CREATE TABLE `category` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR (200) NOT NULL,
    `description` TEXT
);

CREATE TABLE `place` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR (200) NOT NULL,
    `description` TEXT
);

CREATE TABLE IF NOT EXISTS `item` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(200) NOT NULL,
    `category_id` INT NOT NULL,
    `place_id` INT NOT NULL,
    INDEX `FK_category_idx` (`category_id`),
    INDEX `FK_place_idx` (`place_id`),
    CONSTRAINT `FK_category`
		FOREIGN KEY (`category_id`)
        REFERENCES `category` (`id`)
			ON DELETE RESTRICT
            ON UPDATE CASCADE,
		
	CONSTRAINT `FK_place`
		FOREIGN KEY (`place_id`)
        REFERENCES `place` (`id`)
			ON DELETE RESTRICT
            ON UPDATE CASCADE
);

ALTER TABLE `item` ADD `description` TEXT; 

INSERT INTO `category` (`name`, `description`)
VALUES ('Бытовая техника', 'Техника, используемая в бытовых целях'), ('Мебель', 'Диваны, Стулья, столы и прочее'), ('Комп. оборудование', 'Все, что связано с компьютерами');

INSERT INTO `place` (`name`, `description`)
VALUES ('Кабинет директора', 'Личный кабинет директора'), ('Менеджерская', 'Офис менеджеров по продажам, HR и PR менеджеров'), ('Офис', 'Кабинет обслуживания клиентов');

INSERT INTO `item` (`name`, `description`, `category_id`, `place_id`)
VALUES ('Диван', 'Мягкое приспособление, используемое как седалищное средство', '2', '3'), 
			   ('Круглый стол', 'Стол Короля Артура, за которым проходят важные переговоры', '2', '1'), 
			   ('Кондиционер', 'Средство для понижения температуры в помещении', '1', '3'), 
			   ('Ноутбук', 'Компактная, мобильная вычислительная машина', '3', '3'), 
			   ('Ноутбук', 'Компактная, мобильная вычислительная машина', '3', '1'),
			   ('Ноутбук', 'Компактная, мобильная вычислительная машина', '3', '2'), 
			   ('Шкаф', 'Специальный атрибут интерьера, предназначенный для хранения верхней одежды', '2', '2'), 
			   ('Стулья', 'Не мягкое приспособление, используемое как сидалещьное средство', '2', '1'),
               ('Стулья', 'Не мягкое приспособление, используемое как сидалещьное средство', '2', '2'),
               ('Стулья', 'Не мягкое приспособление, используемое как сидалещьное средство', '2', '3')