const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (rq, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage: storage});

const createRouter = (db) => {
    router.get('/:type', (req, res) => {
        db.query("SELECT * FROM `" + req.params.type + "`", (error, results) => {
            // if(error) res.status(400).send(error.sqlMessage);
            res.send(results)
        })
    });

    router.post('/:type', upload.single('image'), (req, res) => {
        const item = req.body;

        if(req.file) {
            item.image = req.file.filename;
        }

        db.query(
            'INSERT INTO `' + req.params.type + '` (`name`, `category_id`, `place_id`, `description`, `image`)' +
            'VALUES (?, ?, ?, ?, ?)',
            [item.name, item.category_id, item.place_id, item.description, item.image],
            (error) => {
                if(error) {
                    res.status(400).send(error.sqlMessage)
                } else {
                    res.send('Item was posted')
                }
            }
        )
    });

    router.get('/:type/:id', (req, res) => {
        db.query('SELECT * FROM `' + req.params.type + '` WHERE id = ' + req.params.id + ';', (error, result) => {
            if(error) {
                res.status(400).send(error.sqlMessage)
            } else {
                res.send(result[0]);
            }
        })
    });

    router.delete('/:type/:id', (req, res) => {
        db.query('DELETE FROM `' + req.params.type + '` WHERE id = ' + req.params.id + ';', (error) => {
            if(error) {
                res.status(400).send(error.sqlMessage);
            } else {
                res.send({msg: 'Delete was successful'})
            }
        })
    });

    router.put('/item/:id', upload.single('image'), (req, res) => {
        const item = req.body;

        if(req.file) {
            item.image = req.file.filename;
        }

        db.query("UPDATE `item` SET `name` = '" + req.body.name + "' , `category_id` = '" + req.body.category_id + "' , `place_id` = '" + req.body.place_id + "' , `description` = '" + req.body.description + `${req.body.image ? "' , `image` = '" + req.body.image : null}` + "' WHERE `id` = '" + req.params.id + "';", (error) => {
            if(error) {
                res.status(400).send(error.sqlMessage)
            } else {
                res.send('item was updated')
            }
        })
    });


    return router
};

module.exports = createRouter;