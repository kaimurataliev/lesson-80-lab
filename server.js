const express = require('express');
const items = require('./app/items');
const mysql = require('mysql');
const cors = require('cors');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'username',
    password: '1234',
    database: 'lesson_80'
});

connection.connect((error) => {

    if (error) {
        console.log(error);
    }

app.use('/', items(connection));

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});
});

